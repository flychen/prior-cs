/*
 * @Author: ly
 * @Date: 2021-07-03 09:02:52
 * @LastEditTime: 2021-07-03 09:07:06
 * @LastEditors: Please set LastEditors
 * @Description: 静态字典配置
 * @FilePath: \md-auth-web\src\utils\staticDictionary.js
 */

export const resourceTypeString = [
  {
    id: 0,
    name: '项目资源',
  },
  {
    id: 1,
    name: '设备资源',
  },
  {
    id: 2,
    name: '产品资源',
  },
  {
    id: 3,
    name: '数据链路资源',
  },
  {
    id: 4,
    name: 'GNSS测站资源',
  },
  {
    id: 5,
    name: 'GNSS基线资源',
  },
  {
    id: 6,
    name: 'GNSS监测点资源',
  },
]
