import enquireJs from 'enquire.js'

export function isDef(v) {
  return v !== undefined && v !== null
}

/**
 * Remove an item from an array.
 */
export function remove(arr, item) {
  if (arr.length) {
    const index = arr.indexOf(item)
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

export function isRegExp(v) {
  return _toString.call(v) === '[object RegExp]'
}

export function enquireScreen(call) {
  const handler = {
    match: function () {
      call && call(true)
    },
    unmatch: function () {
      call && call(false)
    },
  }
  enquireJs.register('only screen and (max-width: 767.99px)', handler)
}

const _toString = Object.prototype.toString

const baseURL = process.env.VUE_APP_BASE_API

// 日期格式化
export function parseTime(time, pattern) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = pattern || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (typeof time === 'string' && /^[0-9]+$/.test(time)) {
      time = parseInt(time)
    } else if (typeof time === 'string') {
      time = time.replace(new RegExp(/-/gm), '/')
    }
    if (typeof time === 'number' && time.toString().length === 10) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay(),
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}

// 表单重置
export function resetForm(refName) {
  if (this.$refs[refName]) {
    this.$nextTick(() => {
      this.$refs[refName].resetFields()
    })
  }
}

// 添加日期范围
export function addDateRange(params, dateRange) {
  var search = params
  search.beginTime = ''
  search.endTime = ''
  if (dateRange != null && dateRange !== '') {
    search.beginTime = dateRange[0]
    search.endTime = dateRange[1]
  }
  return search
}

// 回显数据字典
export function selectDictLabel(datas, value) {
  var actions = []
  Object.keys(datas).some((key) => {
    if (datas[key].dictValue === '' + value) {
      actions.push(datas[key].dictLabel)
      return true
    }
  })
  return actions.join('')
}

// 回显数据字典（字符串数组）
export function selectDictLabels(datas, value, separator) {
  var actions = []
  var currentSeparator = undefined === separator ? ',' : separator
  var temp = value.split(currentSeparator)
  Object.keys(value.split(currentSeparator)).some((val) => {
    Object.keys(datas).some((key) => {
      if (datas[key].dictValue === '' + temp[val]) {
        actions.push(datas[key].dictLabel + currentSeparator)
      }
    })
  })
  return actions.join('').substring(0, actions.join('').length - 1)
}

// 通用下载方法
export function download(fileName) {
  window.location.href =
    baseURL +
    '/common/download?fileName=' +
    encodeURI(fileName) +
    '&delete=' +
    true
}

// 字符串格式化(%s )
export function sprintf(str) {
  var args = arguments
  var flag = true
  var i = 1
  str = str.replace(/%s/g, function () {
    var arg = args[i++]
    if (typeof arg === 'undefined') {
      flag = false
      return ''
    }
    return arg
  })
  return flag ? str : ''
}

// 转换字符串，undefined,null等转化为""
export function praseStrEmpty(str) {
  if (!str || str === 'undefined' || str === 'null') {
    return ''
  }
  return str
}

/**
 * 构造树型结构数据
 * @param {*} data 数据源
 * @param {*} id id字段 默认 'id'
 * @param {*} parentId 父节点字段 默认 'parentId'
 * @param {*} children 孩子节点字段 默认 'children'
 * @param {*} rootId 根Id 默认 0
 */
export function handleTree(data, id, parentId, children, rootId) {
  id = id || 'id'
  parentId = parentId || 'parentId'
  children = children || 'children'
  rootId =
    rootId ||
    Math.min.apply(
      Math,
      data.map((item) => {
        return item[parentId]
      })
    ) ||
    0
  // 对源数据深度克隆
  const cloneData = JSON.parse(JSON.stringify(data))
  // 循环所有项
  const treeData = cloneData.filter((father) => {
    const branchArr = cloneData.filter((child) => {
      // 返回每一项的子级数组
      return father[id] === child[parentId]
    })
    branchArr.length > 0 ? (father.children = branchArr) : ''
    // 返回第一层
    return father[parentId] === rootId
  })
  return treeData !== '' ? treeData : data
}

export function formatBytes(a, b) {
  if (a === 0) return '0 Bytes'
  var c = 1024
  var d = b || 2
  var e = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  var f = Math.floor(Math.log(a) / Math.log(c))
  return parseFloat((a / Math.pow(c, f)).toFixed(d)) + ' ' + e[f]
}

// 防抖
export function debounce(fn, delay) {
  let timeout = null
  return function () {
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      fn.apply(this, arguments)
    }, delay)
  }
}

// 节流
export function throttle(fn, delay) {
  let run = true // 开关
  return function () {
    if (!run) {
      return
    }
    run = false
    setTimeout(() => {
      fn.apply(this, arguments)
      run = true
    }, delay)
  }
}

//度 转  度°分′秒″
export function toDegrees(value) {
  if (typeof value == 'undefined' || value == '') {
    return ''
  }
  value = Math.abs(value)
  var v1 = Math.floor(value) //度
  var v2 = Math.floor((value - v1) * 60) //分
  var v3 = (((value - v1) * 3600) % 60).toFixed(4) //秒(保留4位小数)
  return v1 + '°' + v2 + "'" + v3 + '"'
}

//度°分′秒″ 转  度
export function toDigital(strDu, strFen, strMiao, len) {
  len = len > 8 || typeof len == 'undefined' ? 8 : len //精确到小数点后最多八位
  strDu = typeof strDu == 'undefined' || strDu == '' ? 0 : parseFloat(strDu)
  strFen =
    typeof strFen == 'undefined' || strFen == '' ? 0 : parseFloat(strFen) / 60
  strMiao =
    typeof strMiao == 'undefined' || strMiao == ''
      ? 0
      : parseFloat(strMiao) / 3600
  var digital = strDu + strFen + strMiao
  if (digital == 0) {
    return ''
  } else {
    return digital.toFixed(len)
  }
}

//数组对象排序
export function createComprisonFunction(propertyName) {
  return function (object1, object2) {
    var value1 = object1[propertyName]
    var value2 = object2[propertyName]
    if (value1 < value2) {
      return -1
    } else if (value1 > value2) {
      return 1
    } else {
      return 0
    }
  }
}

//数组对象通过某一个值(某几个值)
export function sortGroupByKeys(data, keys = []) {
  //keys可以传一个数组
  var c = []
  var d = {}
  for (var element of data) {
    let element_keyStr = ''
    let element_key = []
    let element_keyObj = {}
    for (var key of keys) {
      element_key.push(element[key])
      element_keyObj[key] = element[key]
    }
    element_keyStr = element_key.join('_')
    if (!d[element_keyStr]) {
      c.push({
        ...element_keyObj,
        children: [element],
      })
      d[element_keyStr] = element
    } else {
      for (var ele of c) {
        let isTrue = keys.some((key) => {
          return ele[key] != element[key]
        })
        if (!isTrue) {
          ele.children.push(element)
        }
      }
    }
  }
  return c
}

//复制内容到剪贴板
export function copyToBoard(value) {
  const element = document.createElement('textarea')
  document.body.appendChild(element)
  element.value = value
  element.select()
  if (document.execCommand('copy')) {
    document.execCommand('copy')
    document.body.removeChild(element)
    this.$message.success('复制成功')
    return true
  }
  document.body.removeChild(element)
  return false
}
