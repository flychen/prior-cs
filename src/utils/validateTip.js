/**
 * @description form 表单验证自定义提示内容   Key需要与validate.js 函数名匹配
 * @Copyright (c) 2020 medo
 * @author medo_chen
 */

export default {
  isExternal: '请输入正确的外链',
  isPassword: '密码不小于6位',
  isPassword_limit: '以字母开头，长度在8~16之间，只能包含字母、数字和下划线',
  isNumber: '请输入数字',
  isIP: '请输入正确的IP',
  isUrl: '请输入正确的网站链接',
  isLowerCase: '请输入全小写字母',
  isUpperCase: '请输入全大写字母',
  isAlphabets: '请输入以大写字母开头的内容',
  isString: '请输入字符串',
  isArray: '请输入数组类型数据',
  isPort: '请输入正确的端口号',
  isPortLimits: '端口范围为10000~65535',
  isPhone: '请输入正确的手机号码',
  isIdCard: '请输入正确的身份证号（第二代）',
  isEmail: '请输入正确的邮箱',
  isChina: '请输入中文',
  isBlank: '不能为空',
  isTel: '请输入正确的固话',
  numm: '数字且最多两位小数',
  isLongitude: '参考范围(-180.0～+180.0) 例如:45.777',
  isLatitude: '参考范围(-90.0～+90.0) 例如：34.666',
  isRTSP: 'rtsp校验，只要有rtsp://',
  lengthLimit0_6: '长度在 1 到 6 个字符', //验证码长度
  lengthLimit0_20: '长度在 1 到 20 个字符',
  lengthLimit0_30: '长度在 1 到 30 个字符',
  lengthLimit0_41: '长度在 1 到 41 个字符',
  lengthLimit0_50: '长度在 1 到 50 个字符',
  lengthLimit0_100: '长度在 1 到 100 个字符',
  lengthLimit0_500: '长度在 1 到 500 个字符',
  lengthLimit0_10000: '限制数字 0 到 10000',
  lengthLimit0_100000: '限制数字 0 到 100000',
  lengthLimit0_65535: '限制数字 0 到 65535',
  isENU: '保留6位小数，限制 12 个字符',
  isVersion: '版本号格式为XX.XX.XX如:1.1.2/10.1.23',

  default: '未自定义提示内容,请到指定位置添加',
}
