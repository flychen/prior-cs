/*
 * @Author       : archer
 * @Date         : 2021-06-11 14:27:15
 * @LastEditors  : ly
 * @LastEditTime : 2021-08-13 14:24:15
 * @Description  : 描述
 */
function setRem() {
  // 基准大小
  let baseSize = 100,
    baseWidth = 1920,
    baseAdd = 1
  //  根据不同的分辨率改变字体的大小
  // todo 侧边栏与右边的距离缩小一半，去掉阴影
  // 每缩小18px baseSize增加1
  if (window.innerWidth <= baseWidth) {
    baseSize = parseInt((baseWidth - window.innerWidth) / 18) * baseAdd + 100
  } else {
    // 每增大25px baseSize减去1
    baseSize = 100 - parseInt((window.innerWidth - baseWidth) / 25) * baseAdd
  }

  const baseScale = baseSize / 1920 //1920的设计图
  let widthScale = window.innerWidth // 当前窗口的宽度
  const heightScale = window.innerHeight //当前窗口的高度
  // 尺寸换算
  const comparedHeight = (widthScale * 979) / 1920
  if (heightScale < comparedHeight) {
    widthScale = (heightScale * 1920) / 979
  }
  // 计算实际的rem值,得到该宽度下的相应font-size值,并赋予给html的font-size,
  const rem = widthScale * baseScale
  document.documentElement.style.fontSize = `${rem}px`
}
// 初始化
// setRem()

export { setRem }
