/*
 * @Author       : ly
 * @Date         : 2021-07-13 09:17:34
 * @LastEditors  : ly
 * @Description  : 全局过滤器
 */
import { resourceTypeString } from '@/utils/staticDictionary'

//资源组类型
const resourceTypeFilter = (index) => {
  return resourceTypeString[index] ? resourceTypeString[index].name : '--'
}

//表格编号排序
const filterIndex = (value) => {
  if (value < 10) {
    let val = value.toString()
    return val.padStart(2, '0')
  } else {
    return value
  }
}

export { resourceTypeFilter, filterIndex }
