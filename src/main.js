/*
 * @Author: your name
 * @Date: 2021-06-30 13:43:02
 * @LastEditTime : 2021-11-12 09:17:22
 * @LastEditors  : archer
 * @Description: In User Settings Edit
 * @FilePath: \md-auth-web\src\main.js
 */
import Vue from 'vue'
import App from './App.vue'
import { initRouter } from './router'
import './theme/index.less'
import Antd from 'ant-design-vue'
// import '@/mock'
import store from './store'
import 'animate.css/source/animate.css'
import Plugins from '@/plugins'
import bootstrap from '@/bootstrap'
import 'moment/locale/zh-cn'
import './assets/icons' // icon

import '@/assets/styles/ant.less'
import '@/assets/styles/index.less' // global style
import { setRem } from '@/utils/rem'

import CodeEditor from 'bin-code-editor'
import 'bin-code-editor/lib/styles/index.css'

import * as filters from '@/utils/globalfilter'

import '@grapecity/activereports/styles/ar-js-ui.css'
import '@grapecity/activereports/styles/ar-js-viewer.css'
import '@grapecity/activereports/styles/ar-js-designer.css'

//全局过滤器
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})

const router = initRouter(store.state.setting.asyncRoutes)

Vue.use(Antd)
Vue.config.productionTip = false
Vue.use(Plugins)
Vue.use(CodeEditor)

setRem()

bootstrap({ router, store, message: Vue.prototype.$message })

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
