const config = {
  401: {
    img: require('@/assets/error_images/401.png'),
    headline: '没有操作权限...',
    desc: '您当前帐号没有操作权限,请联系管理员。',
  },
  403: {
    img: require('@/assets/error_images/403.png'),
    headline: '请求被拒绝...',
    desc: '您当前请求被服务拒绝,请联系管理员。',
  },
  404: {
    img: require('@/assets/error_images/404.png'),
    headline: '当前页面不存在...',
    desc: ' 您当前访问的页面不存在或仍在开发中',
  },
}

export default config
