import TabsView from '@/layouts/tabs/TabsView'
// import BlankView from '@/layouts/BlankView'
import PageView from '@/layouts/PageView'

// 路由配置
const options = {
  routes: [
    {
      path: '/login',
      name: '登录页',
      component: () => import('@/pages/login'),
    },
    {
      path: '/404',
      name: '404',
      component: () => import('@/pages/exception/404'),
    },
    {
      path: '/403',
      name: '403',
      component: () => import('@/pages/exception/403'),
    },
    {
      path: '/',
      name: '首页',
      component: TabsView,
      children: [
        {
          path: 'BIM',
          name: 'BIM',
          meta: {
            icon: 'medo_access',
            authority: {
              permission: 'user:DescribeCompany',
            },
          },
          component: PageView,
          children: [
            {
              path: '3DBIM',
              name: '3D建模',
              meta: {
                icon: 'medo_point',
                authority: {
                  permission: 'user:ListUser',
                },
              },
              component: () => import('@/pages/BIM'),
            },
          ],
        },
        {
          path: 'charts',
          name: '图表',
          meta: {
            icon: 'medo_access',
            authority: {
              permission: 'user:DescribeCompany',
            },
          },
          component: PageView,
          children: [
            {
              path: 'echart',
              name: 'echart图表',
              meta: {
                icon: 'medo_point',
                authority: {
                  permission: 'user:ListUser',
                },
              },
              component: () => import('@/pages/charts'),
            },
          ],
        },
        {
          path: 'report',
          name: '报表',
          meta: {
            icon: 'medo_access',
            authority: {
              permission: 'user:DescribeCompany',
            },
          },
          component: PageView,
          children: [
            {
              path: 'export',
              name: '报表预览与导出',
              meta: {
                icon: 'medo_point',
                authority: {
                  permission: 'user:ListUser',
                },
              },
              component: () => import('@/pages/report'),
            },
          ],
        },
        {
          path: '2D',
          name: '2D配置',
          meta: {
            icon: 'medo_access',
            authority: {
              permission: 'user:DescribeCompany',
            },
          },
          component: PageView,
          children: [
            {
              path: 'config',
              name: '底图配置',
              meta: {
                icon: 'medo_point',
                authority: {
                  permission: 'user:ListUser',
                },
              },
              component: () => import('@/pages/2D'),
            },
          ],
        },
        {
          path: 'panorama',
          name: '全景',
          meta: {
            icon: 'medo_access',
            authority: {
              permission: 'user:DescribeCompany',
            },
          },
          component: PageView,
          children: [
            {
              path: 'show',
              name: '全景',
              meta: {
                icon: 'medo_point',
                authority: {
                  permission: 'user:ListUser',
                },
              },
              component: () => import('@/pages/panorama'),
            },
          ],
        },
        {
          path: 'map',
          name: '地图',
          meta: {
            icon: 'medo_access',
            authority: {
              permission: 'user:DescribeCompany',
            },
          },
          component: PageView,
          children: [
            {
              path: 'gaode',
              name: '高德地图',
              meta: {
                icon: 'medo_point',
                authority: {
                  permission: 'user:ListUser',
                },
              },
              component: () => import('@/pages/map'),
            },
          ],
        },
      ],
    },
    {
      path: '*',
      name: '404',
      redirect: '/404',
      invisible: true,
    },
  ],
}

export default options
