import { hasAuthority } from '@/utils/authority-utils'
import { loginIgnore } from '@/router/index'
import { checkAuthorization } from '@/utils/request'
import { logout } from '@/services/login/auth'
import NProgress from 'nprogress'

NProgress.configure({ showSpinner: false })

/**
 * 进度条开始
 * @param to
 * @param form
 * @param next
 */
const progressStart = (to, from, next) => {
  // start progress bar
  if (!NProgress.isStarted()) {
    NProgress.start()
  }
  next()
}

/**
 * 登录守卫
 * @param to
 * @param form
 * @param next
 * @param options
 */
const loginGuard = (to, from, next, options) => {
  const { message } = options
  if (!loginIgnore.includes(to) && !checkAuthorization()) {
    if (from.fullPath != '/') {
      message.warning('登录已失效，请重新登录')
    }
    next({ path: '/login' })
  } else if (loginIgnore.paths.includes(to.path) && checkAuthorization()) {
    //path=login,token 未失效,直接进入主页面
    next({ path: '/' })
    NProgress.done()
  } else {
    next()
    const pending = options.router.history.pending
    if (pending && pending.path == from.path) {
      NProgress.done()
    }
  }
}

/**
 * 权限守卫
 * @param to
 * @param form
 * @param next
 * @param options
 */
const authorityGuard = (to, from, next, options) => {
  const { store, message } = options
  const permissions = store.getters['account/permissions']
  const roles = store.getters['account/roles']
  if (!hasAuthority(to, permissions, roles)) {
    message.warning(`对不起，您无权访问页面: ${to.fullPath}，请联系管理员`)
    next({ path: '/403' })
    //TODO 同一个平台多端口在同设备浏览器运行时，让其重新登录
    logout()
    // NProgress.done()
  } else {
    next()
  }
}

/**
 * 混合导航模式下一级菜单跳转重定向
 * 侧边导航模式默认跳转至第一组子菜单
 * @param to
 * @param from
 * @param next
 * @param options
 * @returns {*}
 */
const redirectGuard = (to, from, next, options) => {
  const { store } = options
  const getFirstChild = (routes) => {
    const route = routes[0]
    if (!route.children || route.children.length === 0) {
      return route
    }
    return getFirstChild(route.children)
  }
  //混合导航模式
  if (store.state.setting.layout === 'mix') {
    const firstMenu = store.getters['setting/firstMenu']
    if (firstMenu.find((item) => item.fullPath === to.fullPath)) {
      store.commit('setting/setActivatedFirst', to.fullPath)
      const subMenu = store.getters['setting/subMenu']
      if (subMenu.length > 0) {
        const redirect = getFirstChild(subMenu)
        return next({ path: redirect.fullPath })
      }
    }
  }
  //侧边导航模式
  else if (store.state.setting.layout === 'side') {
    const firstMenu = store.getters['setting/firstMenu']
    if (to.fullPath == '/' && firstMenu && firstMenu.length > 0) {
      store.commit('setting/setActivatedFirst', firstMenu[0].fullPath)
      const subMenu = store.getters['setting/subMenu']
      if (subMenu.length > 0) {
        const redirect = getFirstChild(subMenu)
        return next({ path: redirect.fullPath })
      }
    }
  }
  next()
}

/**
 * 进度条结束
 * @param to
 * @param form
 * @param options
 */
const progressDone = () => {
  // finish progress bar
  NProgress.done()
}

export default {
  beforeEach: [progressStart, loginGuard, authorityGuard, redirectGuard],
  afterEach: [progressDone],
}
