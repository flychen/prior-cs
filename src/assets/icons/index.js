import Vue from 'vue'
import medoIcon from '@/components/medoIcon' // svg component

// register globally
Vue.component('medoIcon', medoIcon)

const req = require.context('./svg', false, /\.svg$/)
const requireAll = (requireContext) => requireContext.keys().map(requireContext)
requireAll(req)
