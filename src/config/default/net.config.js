/**
 * @description 默认网路配置
 **/
const network = {
  //后端数据的接收方式
  contentType: 'application/json;charset=UTF-8',
  //跨域认证信息 header 名
  xsrfHeaderName: 'Authorization',
  // CookieName 名
  xsrfCookieName: 'Auth-Authorization',
  //请求服务方式
  accessType: 'web',
  //最长请求时间
  requestTimeout: 5000,
  //操作正常code，支持String、Array、int多种类型
  successCode: [200, 0],
  // 查询企业列表所用token
  authorization: '',
  //特殊接口header认证信息
  appKey: 'a4848800-888e-4bc5-afe8-3ccc3520524a',
  appSecret: 'af2dfe22-aa44-4e47-975e-76a61ff66d76',

  // loading 过滤配置
  // debounce: ['Update', 'Add'],
  debounce: ['Query'],

  // 无权限接口拦截，清空Authorization
  noPermissionFilter: ['/SendSmsCode', '/SignIn', '/SmsLogin'],
}
module.exports = network
