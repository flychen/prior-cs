const ANTD = require('./antd.config')
const ADMIN = require('./admin.config')
const ANIMATE = require('./animate.config')
const setting = require('./setting.config')
const net = require('./net.config')

module.exports = { ANTD, ADMIN, ANIMATE, setting, net }
