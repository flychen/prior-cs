// 自定义配置，参考 ./default/setting.config.js，需要自定义的属性在这里配置即可
module.exports = {
  theme: {
    // color: '#13c2c2',
    color: '#47BA8E',
    mode: 'light',
  },
  componentSize: 'small', //default  large  small
  multiPage: false,
  asyncRoutes: false,
  fixedHeader: true,
  animate: {
    name: 'fade',
    direction: 'left',
  },
}
