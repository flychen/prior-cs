/*
 * @Author       : zp
 * @Date         : 2021-06-21 11:14:33
 * @LastEditors  : ly
 * @LastEditTime : 2021-07-27 13:20:03
 * @Description  : 描述
 */
import {
  QueryResourceGroupList,
  QueryResourceGroup,
  DeleteResourceGroup,
  UpdateResourceGroup,
  QueryResourceInGroup,
  QueryResourceInGroupList,
  AddResourceGroup,
  ResourceTransfer,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 查询公司内的资源组列表(不分页列表)
 * @param {*} companyID
 * @param {*} name
 * @param {*} pageSize
 * @param {*} currentPage
 * @returns
 */
export async function queryResourceGroupList(body) {
  return request(QueryResourceGroupList, METHOD.POST, body)
}

/**
 * 查询公司内的资源组列表(分页列表)
 * @param {*} companyID
 * @param {*} resourceGroupName
 * @param {*} pageSize
 * @param {*} currentPage
 * @returns
 */
export async function queryResourceGroup(body) {
  return request(QueryResourceGroup, METHOD.POST, body)
}

/**
 * 删除资源组, 如果资源组下有资源，则无法删除
 * @param {*} companyID
 * @param {Int[]} resourceGroupIDList 资源组编号列表
 * @returns
 */
export async function deleteResourceGroup(body) {
  return request(DeleteResourceGroup, METHOD.POST, body)
}

/**
 * 更新资源组
 * @param {*} companyID
 * @param {*} resourceGroupID 资源组编号
 * @param {*} resourceGroupName 资源组名称
 * @param {*} resourceGroupDesc 资源组描述
 *
 * @returns
 */
export async function updateResourceGroup(body) {
  return request(UpdateResourceGroup, METHOD.POST, body)
}

/**
 * 查询公司内的资源组內列表(分页)
 * @param {*} companyID
 * @param {*} groupID 可选 资源组ID
 * @param {*} resourceToken 可选 资源标识，支持模糊查询
 * @param {*} resourceToken 可选 资源标识，支持模糊查询
 * @param {*} resourceType 可选 资源标识，支持模糊查询
 * @param {*} pageSize
 * @param {*} currentPage
 * @returns
 */
export async function queryResourceInGroup(body) {
  return request(QueryResourceInGroup, METHOD.POST, body)
}

/**
 * 查询公司内的资源组內列表(不分页)
 * @param {*} companyID
 * @param {*} groupID 可选 资源组ID
 * @param {*} resourceToken 可选 资源标识，支持模糊查询
 * @param {*} resourceToken 可选 资源标识，支持模糊查询
 * @param {*} resourceType 可选 资源标识，支持模糊查询
 * @param {*} pageSize
 * @param {*} currentPage
 * @returns
 */
export async function queryResourceInGroupList(body) {
  return request(QueryResourceInGroupList, METHOD.POST, body)
}

/**
 * 添加资源组
 * @param {*} companyID
 * @param {*} resourceGroupName 资源组名称
 * @param {*} resourceGroupDesc 资源组描述
 * @returns
 */
export async function addResourceGroup(body) {
  return request(AddResourceGroup, METHOD.POST, body)
}

/**
 * 将资源从本公司的一个资源组，转入本公司的另一个资源组
 * @param {*} companyID
 * @param {*} resourceIDList 资源编号列表
 * @param {*} targetResourceGroupID 本公司内的转入资源组编号
 * @returns
 */
export async function resourceTransfer(body) {
  return request(ResourceTransfer, METHOD.POST, body)
}
