/*
 * @Author       : archer
 * @Date         : 2021-06-21 11:11:03
 * @LastEditors  : archer
 * @LastEditTime : 2021-11-12 14:10:56
 * @Description  : 米度用户权限服务接口
 */
const AUTH_URL = process.env.VUE_APP_BASE_API + '/auth'
module.exports = {
  QueryUserInCompany: `${AUTH_URL}/QueryUserInCompany`,
  SignIn: `${AUTH_URL}/SignIn`,
  SmsLogin: `${AUTH_URL}/SmsLogin`,
  ROUTES: `${AUTH_URL}/routes`,
  QueryUserByID: `${AUTH_URL}/QueryUserByID`,
  GetUserByToken: `${AUTH_URL}/GetUserByToken`,
  SendSmsCode: `${AUTH_URL}/SendSmsCode`,
  UploadUserAvatar: `${AUTH_URL}/UploadUserAvatar`,

  QueryUser: `${AUTH_URL}/QueryUser`,
  QueryUserBatch: `${AUTH_URL}/QueryUserBatch`,
  AddUser: `${AUTH_URL}/AddUser`,
  UpdateUser: `${AUTH_URL}/UpdateUser`,
  DeleteUser: `${AUTH_URL}/DeleteUser`,
  ResetPassword: `${AUTH_URL}/ResetPassword`,

  QueryExternalPerson: `${AUTH_URL}/QueryExternalPerson`,
  DeleteCompanyExternalUser: `${AUTH_URL}/DeleteCompanyExternalUser`,
  BatchAddCompanyExternalUser: `${AUTH_URL}/BatchAddCompanyExternalUser`,
  AddCompanyExternalUser: `${AUTH_URL}/AddCompanyExternalUser`,

  QueryResourceGroupList: `${AUTH_URL}/QueryResourceGroupList`,
  DeleteResourceGroup: `${AUTH_URL}/DeleteResourceGroup`,
  UpdateResourceGroup: `${AUTH_URL}/UpdateResourceGroup`,
  QueryResourceInGroup: `${AUTH_URL}/QueryResourceInGroup`,
  QueryResourceInGroupList: `${AUTH_URL}/QueryResourceInGroupList`,
  QueryResourceGroup: `${AUTH_URL}/QueryResourceGroup`,
  AddResourceGroup: `${AUTH_URL}/AddResourceGroup`,
  ResourceTransfer: `${AUTH_URL}/ResourceTransfer`,
  ManagerUserInGroup: `${AUTH_URL}/ManagerUserInGroup`,

  AddCompany: `${AUTH_URL}/AddCompany`,
  UpdateCompany: `${AUTH_URL}/UpdateCompany`,
  DeleteCompany: `${AUTH_URL}/DeleteCompany`,
  GetCompanyInfo: `${AUTH_URL}/GetCompanyInfo`,

  QueryCompanyOrganization: `${AUTH_URL}/QueryCompanyOrganization`,
  QueryPermissionGroup: `${AUTH_URL}/QueryPermissionGroup`,
  QueryPermissionGroupList: `${AUTH_URL}/QueryPermissionGroupList`,
  DeletePermissionGroup: `${AUTH_URL}/DeletePermissionGroup`,
  UpdatePermissionGroup: `${AUTH_URL}/UpdatePermissionGroup`,
  AddPermissionGroup: `${AUTH_URL}/AddPermissionGroup`,
  QueryAllPermissionInService: `${AUTH_URL}/QueryAllPermissionInService`,
  QueryUserInCompanyList: `${AUTH_URL}/QueryUserInCompanyList`,
  QueryPermissionGroupInfo: `${AUTH_URL}/QueryPermissionGroupInfo`,

  AddDepartment: `${AUTH_URL}/AddDepartment`,
  DeleteDepartment: `${AUTH_URL}/DeleteDepartment`,
  UpdateDepartment: `${AUTH_URL}/UpdateDepartment`,
  QueryPermission: `${AUTH_URL}/QueryPermission`,
  DeletePermission: `${AUTH_URL}/DeletePermission`,
  GetResourceTypeList: `${AUTH_URL}/GetResourceTypeList`,
  AddPermission: `${AUTH_URL}/AddPermission`,
  GetService: `${AUTH_URL}/GetService`,
  UpdatePermission: `${AUTH_URL}/UpdatePermission`,
  QueryPermissionDetail: `${AUTH_URL}/QueryPermissionDetail`,
  QueryPermissionStrategy: `${AUTH_URL}/QueryPermissionStrategy`,
  DeletePermissionStrategy: `${AUTH_URL}/DeletePermissionStrategy`,
  UploadPermissionStrategy: `${AUTH_URL}/UploadPermissionStrategy`,
  AddPermissionStrategy: `${AUTH_URL}/AddPermissionStrategy`,
  UpdatePermissionStrategy: `${AUTH_URL}/UpdatePermissionStrategy`,
  QueryPermissionStrategyList: `${AUTH_URL}/QueryPermissionStrategyList`,
  ManageStrategyGroup: `${AUTH_URL}/ManageStrategyGroup`,
  QueryPermissionStrategyInfo: `${AUTH_URL}/QueryPermissionStrategyInfo`,

  QueryDepartmentOrganization: `${AUTH_URL}/QueryDepartmentOrganization`,
  QueryPermissionList: `${AUTH_URL}/QueryPermissionList`,

  QueryApplication: `${AUTH_URL}/QueryApplication`,
  QueryApplicationList: `${AUTH_URL}/QueryApplicationList`,
  AddApplication: `${AUTH_URL}/AddApplication`,
  DeleteApplication: `${AUTH_URL}/DeleteApplication`,
  UpdateApplication: `${AUTH_URL}/UpdateApplication`,
  ManageApplication: `${AUTH_URL}/ManageApplication`,
}
