/*
 * @Author       : ly
 * @Date         : 2021-07-14 15:37:47
 * @LastEditors  : archer
 * @Description  : 描述
 */
const BFF_URL = process.env.VUE_APP_BASE_API + '/webbff'
module.exports = {
  BUpdateDepartment: `${BFF_URL}/departmentManage/UpdateDepartment`,
  BAddDepartment: `${BFF_URL}/departmentManage/AddDepartment`,
}
