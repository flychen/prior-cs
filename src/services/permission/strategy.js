/*
 * @Author       : zp
 * @Date         : 2021-07-02 15:00:07
 * @LastEditors  : ly
 * @LastEditTime : 2021-07-22 13:29:55
 * @Description  : 权限策略
 */

import {
  QueryPermissionStrategy,
  DeletePermissionStrategy,
  UpdatePermissionStrategy,
  QueryPermissionStrategyList,
  ManageStrategyGroup,
  AddPermissionStrategy,
  QueryPermissionStrategyInfo,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 分页查询设备管理列表
 * @param companyID 账户名
 * @param currentPage 账户密码
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function queryPermissionStrategy(body) {
  return request(QueryPermissionStrategy, METHOD.POST, body)
}

/**
 * 删除权限策略
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function deletePermissionStrategy(body) {
  return request(DeletePermissionStrategy, METHOD.POST, body)
}

/**
 * 权限策略模块 - 修改权限策略
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function updatePermissionStrategy(body) {
  return request(UpdatePermissionStrategy, METHOD.POST, body)
}

/**
 * 分权限策略模块 - 添加权限策略
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function addPermissionStrategy(body) {
  return request(AddPermissionStrategy, METHOD.POST, body)
}

/**
 * 查询权限策略列表
 * @param companyID 公司编号
 * @param strategyIDList  可选 权限ID列表
 * @param strategyName  可选 权限策略名称,支持模糊查询
 * @param strategyVersion  可选权限策略版本
 * @param createType 可选 创建类型
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function queryPermissionStrategyList(body) {
  return request(QueryPermissionStrategyList, METHOD.POST, body)
}

/**
 * 查询权限策略列表
 * @param companyID 公司编号
 * @param groupID 权限分组编号
 * @param addStrategyIDList  可选 权限分组添加策略编号列表
 * @param removeStrategyIDList  权限分组移除策略编号列表
 * @param strategyResource 可选 策略资源(5000)，允许的值为 *(所有),none(无), 1,2,3
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function manageStrategyGroup(body) {
  return request(ManageStrategyGroup, METHOD.POST, body)
}

/**
 * 查询权限策略详情
 * @param companyID 公司编号
 * @param strategyID 权限Id
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function queryPermissionStrategyInfo(body) {
  return request(QueryPermissionStrategyInfo, METHOD.POST, body)
}
