/*
 * @Author       : zp
 * @Date         : 2021-07-02 11:12:18
 * @LastEditors  : zp
 * @LastEditTime : 2021-07-02 17:15:12
 * @Description  : 系统权限模块
 */
import {
  QueryPermission,
  DeletePermission,
  AddPermission,
  UpdatePermission,
  QueryPermissionDetail,
  QueryPermissionList,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 分页查询设备管理列表
 * @param companyID 账户名
 * @param currentPage 账户密码
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function queryPermission(body) {
  return request(QueryPermission, METHOD.POST, body)
}

/**
 * 分页查询设备管理列表
 * @param companyID 账户名
 * @param currentPage 账户密码
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function deletePermission(body) {
  return request(DeletePermission, METHOD.POST, body)
}

/**
 * 分页查询设备管理列表
 * @param companyID 账户名
 * @param currentPage 账户密码
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function addPermission(body) {
  return request(AddPermission, METHOD.POST, body)
}

/**
 * 分页查询设备管理列表
 * @param companyID 账户名
 * @param currentPage 账户密码
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function updatePermission(body) {
  return request(UpdatePermission, METHOD.POST, body)
}

/**
 * 分页查询设备管理列表
 * @param companyID 账户名
 * @param currentPage 账户密码
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function queryPermissionDetail(body) {
  return request(QueryPermissionDetail, METHOD.POST, body)
}

/**
 * 分页查询设备管理列表
 * @param companyID 账户名
 * @param currentPage 账户密码
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function queryPermissionList(body) {
  return request(QueryPermissionList, METHOD.POST, body)
}
