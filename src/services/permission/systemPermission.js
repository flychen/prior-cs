/*
 * @Author: ly
 * @Date: 2021-07-02 15:20:44
 * @LastEditTime : 2021-07-05 18:03:53
 * @LastEditors  : archer
 * @Description: 系统权限管理api
 * @FilePath: \md-auth-web\src\services\permission\systemPermission.js
 */

import {
  QueryPermission,
  QueryPermissionDetail,
  AddPermission,
  DeletePermission,
  UpdatePermission,
  QueryAllPermissionInService,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 添加权限
 * @param {*} body
 * @returns
 */
export async function addPermission(body) {
  return request(AddPermission, METHOD.POST, body)
}

/**
 * 删除权限
 * @param {*} body
 * @returns
 */
export async function deletePermission(body) {
  return request(DeletePermission, METHOD.POST, body)
}

/**
 * 修改权限
 * @param {*} body
 * @returns
 */
export async function updatePermission(body) {
  return request(UpdatePermission, METHOD.POST, body)
}

/**
 * 查询权限列表
 * @param {*} body
 * @returns
 */
export async function queryPermission(body) {
  return request(QueryPermission, METHOD.POST, body)
}

/**
 * 查询权限详情
 * @param {*} body
 * @returns
 */
export async function queryPermissionDetail(body) {
  return request(QueryPermissionDetail, METHOD.POST, body)
}

/**
 * 查询用户在某公司某服务中的所有权限
 * @param {*} body
 * @returns
 */
export async function queryAllPermissionInService(body) {
  return request(QueryAllPermissionInService, METHOD.POST, body)
}
