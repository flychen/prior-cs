/*
 * @Author       : archer
 * @Date         : 2021-06-21 11:14:33
 * @LastEditors  : ly
 * @LastEditTime : 2021-07-28 14:20:33
 * @Description  : 登录、校验
 */
import {
  SignIn,
  SmsLogin,
  ROUTES,
  QueryUserByID,
  UpdateUser,
  GetUserByToken,
} from '@/services/auth'
import { request, METHOD, removeAuthorization } from '@/utils/request'

export async function getRoutesConfig() {
  return request(ROUTES, METHOD.GET)
}

export async function queryUserByID(body) {
  return request(QueryUserByID, METHOD.POST, body)
}

export async function updateUser(body) {
  return request(UpdateUser, METHOD.POST, body)
}

export async function getUserByToken() {
  return request(GetUserByToken, METHOD.GET)
}

export async function signIn(body) {
  return request(SignIn, METHOD.POST, body)
}

export async function smsLogin(body) {
  return request(SmsLogin, METHOD.POST, body)
}

/**
 * 退出登录
 */
export async function logout() {
  localStorage.removeItem(process.env.VUE_APP_ROUTES_KEY)
  localStorage.removeItem(process.env.VUE_APP_PERMISSIONS_KEY)
  localStorage.removeItem(process.env.VUE_APP_ROLES_KEY)
  removeAuthorization()
}
