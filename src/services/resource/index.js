/*
 * @Author       : zp
 * @Date         : 2021-07-02 11:29:24
 * @LastEditors  : ly
 * @LastEditTime : 2021-07-08 11:21:38
 * @Description  : 资源模块
 */
import { GetResourceTypeList, GetService } from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 资源管理模块 - 查询资源类型列表
 * @param companyID 账户名
 * @param currentPage 账户密码
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function getResourceTypeList() {
  return request(GetResourceTypeList, METHOD.GET)
}

/**
 * 通用模块 - 获取所有服务
 * @param serviceName 服务名称
 * @param serviceDesc 服务别名
 * @param pageSize
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function getService() {
  return request(GetService, METHOD.GET)
}
