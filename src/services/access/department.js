/*
 * @Author       : zp
 * @Date         : 2021-06-21 11:14:33
 * @LastEditors  : ly
 * @LastEditTime : 2021-07-14 15:50:41
 * @Description  : 描述
 */
import {
  AddDepartment,
  DeleteDepartment,
  UpdateDepartment,
  QueryDepartmentOrganization,
} from '@/services/auth'
import { BUpdateDepartment, BAddDepartment } from '@/services/webbff'
import { request, METHOD } from '@/utils/request'

/**
 * 添加部门
 * @param {*} body
 * @returns
 */
export async function addDepartment(body) {
  return request(AddDepartment, METHOD.POST, body)
}

/**
 * 添加部门(批量 web_bfff)
 * @param {*} body
 * @returns
 */
export async function addDepartments(body) {
  return request(BAddDepartment, METHOD.POST, body)
}

/**
 * 删除部门
 * @param {*} body
 * @returns
 */
export async function deleteDepartment(body) {
  return request(DeleteDepartment, METHOD.POST, body)
}

/**
 * 修改部门
 * @param {*} body
 * @returns
 */
export async function updateDepartment(body) {
  return request(UpdateDepartment, METHOD.POST, body)
}

/**
 * 修改部门(批量 web_bfff)
 * @param {*} body
 * @returns
 */
export async function updateDepartments(body) {
  return request(BUpdateDepartment, METHOD.POST, body)
}

/**
 * 获取部门的组织架构
 * @param {*} body
 * @returns
 */
export async function queryDepartmentOrganization(body) {
  return request(QueryDepartmentOrganization, METHOD.POST, body)
}
