/*
 * @Author       : zp
 * @Date         : 2021-06-21 11:14:33
 * @LastEditors  : ly
 * @LastEditTime : 2021-07-16 14:40:08
 * @Description  : 描述
 */
import {
  AddCompany,
  UpdateCompany,
  DeleteCompany,
  GetCompanyInfo,
  QueryCompanyOrganization,
  QueryUserInCompanyList,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 添加公司
 * @param {*} body
 * @returns
 */
export async function addCompany(body) {
  return request(AddCompany, METHOD.POST, body)
}

/**
 * 修改公司信息
 * @param {*} body
 * @returns
 */
export async function updateCompany(body) {
  return request(UpdateCompany, METHOD.POST, body)
}

/**
 * 删除公司信息
 * @param {*} body
 * @returns
 */
export async function deleteCompany(body) {
  return request(DeleteCompany, METHOD.POST, body)
}

/**
 * 获取公司的组织结构
 * @param {*} body
 * @returns
 */
export async function queryCompanyOrganization(body) {
  return request(QueryCompanyOrganization, METHOD.POST, body)
}

/**
 * 查询用户所在的所有公司列表
 * @param {*} body
 * @returns
 */
export async function queryUserInCompanyList(body) {
  return request(QueryUserInCompanyList, METHOD.POST, body)
}

/**
 * 获取公司信息
 * @param {*} body
 * @returns
 */
export async function getCompanyInfo(body) {
  return request(GetCompanyInfo, METHOD.POST, body)
}
