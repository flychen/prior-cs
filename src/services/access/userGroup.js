/*
 * @Author       : archer
 * @Date         : 2021-06-21 11:14:33
 * @LastEditors  : ly
 * @LastEditTime : 2021-07-22 10:10:55
 * @Description  : 描述
 */
import {
  QueryUser,
  QueryPermissionGroup,
  QueryPermissionGroupList,
  DeletePermissionGroup,
  UpdatePermissionGroup,
  AddPermissionGroup,
  ManagerUserInGroup,
  QueryPermissionGroupInfo,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 创建单个设备
 * @param  {Int} companyID 公司ID
 * @param  {String} departmentName 可选 部门名称, 支持模糊匹配
 * @param  {String} userName  可选 用户名, 支持模糊匹配
 * @param  {String} cellPhone  可选 用户手机号码, 支持模糊匹配
 * @param  {Boolean} includeExternal 可选 是否包含外部用户
 * @param  {Int} pageSize
 * @param  {String} currentPage
 * @return {Promise<AxiosResponse<T>>}
 */
export async function queryUser(body) {
  return request(QueryUser, METHOD.POST, body)
}

/**
 * 创建查询公司用户组(分页)
 * @param  {Int} companyID 公司编号公司编号
 * @param  {Int} groupName 权限分组名称,支持模糊查询
 * @param  {Int} createType 创建类型
 * @param  {Int} pageSize
 * @param  {String} currentPage
 * @return {Promise<AxiosResponse<T>>}
 */
export async function queryPermissionGroup(body) {
  return request(QueryPermissionGroup, METHOD.POST, body)
}

/**
 * 创建查询公司用户组(不分页)
 * @param  {Int} companyID 公司编号公司编号
 * @param  {Int} groupName 权限分组名称,支持模糊查询
 * @param  {Int} createType 创建类型
 * @return {Promise<AxiosResponse<T>>}
 */
export async function queryPermissionGroupList(body) {
  return request(QueryPermissionGroupList, METHOD.POST, body)
}

/**
 * 删除用户组
 * @param  {Int} companyID 公司编号公司编号
 * @param  {Int} groupIDList 用户组编号列表
 * @return {Promise<AxiosResponse<T>>}
 */
export async function deletePermissionGroup(body) {
  return request(DeletePermissionGroup, METHOD.POST, body)
}
/**
 * 添加用户组
 * @param  {Int} companyID 公司编号
 * @param  {Int} groupName 用户组名称(100)
 * @param  {Int} groupDesc 用户组描述(500)
 * @param  {Int} displayOrder 显示顺序
 * @return {Promise<AxiosResponse<T>>}
 */
export async function addPermissionGroup(body) {
  return request(AddPermissionGroup, METHOD.POST, body)
}

/**
 * 修改用户组
 * @param  {Int} companyID 公司编号
 * @param  {Int} groupID 用户组编号
 * @param  {Int} groupName 用户组名称(100)
 * @param  {Int} groupDesc 用户组描述(500)
 * @param  {Int} displayOrder 显示顺序
 * @return {Promise<AxiosResponse<T>>}
 */
export async function updatePermissionGroup(body) {
  return request(UpdatePermissionGroup, METHOD.POST, body)
}

/**
 * 修改用户组
 * @param  {Int} companyID 公司编号
 * @param  {Int} groupID 用户组编号
 * @param  {Int} groupName 用户组名称(100)
 * @param  {Int} groupDesc 用户组描述(500)
 * @param  {Int} displayOrder 显示顺序
 * @return {Promise<AxiosResponse<T>>}
 */
export async function managerUserInGroup(body) {
  return request(ManagerUserInGroup, METHOD.POST, body)
}

/**
 * 查询公司用户组详情
 * @param  {Int} companyID 公司编号
 * @param  {Int} groupID 用户组编号
 * @return {Promise<AxiosResponse<T>>}
 */
export async function queryPermissionGroupInfo(body) {
  return request(QueryPermissionGroupInfo, METHOD.POST, body)
}
