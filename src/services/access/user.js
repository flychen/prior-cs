/*
 * @Author       : archer
 * @Date         : 2021-06-21 11:14:33
 * @LastEditors  : archer
 * @LastEditTime : 2021-07-08 11:25:36
 * @Description  : 描述
 */

import {
  QueryUser,
  QueryUserBatch,
  AddUser,
  UpdateUser,
  DeleteUser,
  ResetPassword,
  SendSmsCode,
  UploadUserAvatar,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 查询系统用户
 * @param  {Int} companyID 公司ID
 * @param  {String} departmentName 可选 部门名称, 支持模糊匹配
 * @param  {String} userName  可选 用户名, 支持模糊匹配
 * @param  {String} cellPhone  可选 用户手机号码, 支持模糊匹配
 * @param  {Boolean} includeExternal 可选 是否包含外部用户
 * @param  {Int} pageSize
 * @param  {String} currentPage
 * @return {Promise<AxiosResponse<T>>}
 */
export async function queryUser(body) {
  return request(QueryUser, METHOD.POST, body)
}

/**
 * 根据公司查询用户
 * @param  {Int} companyID 公司ID
 * @param  {int[]} userIDList 用户id列表,[1-100]
 * @return {Promise<AxiosResponse<T>>}
 */
export async function queryUserBatch(body) {
  return request(QueryUserBatch, METHOD.POST, body)
}

/**
 * 添加用户
 * @param  {Int} companyID 公司ID
 * @param  {String} departmentName 可选 部门名称, 支持模糊匹配
 * @param  {String} userName  可选 用户名, 支持模糊匹配
 * @param  {String} cellPhone  可选 用户手机号码, 支持模糊匹配
 * @param  {Boolean} includeExternal 可选 是否包含外部用户
 * @param  {Int} pageSize
 * @param  {String} currentPage
 * @return {Promise<AxiosResponse<T>>}
 */
export async function addUser(body) {
  return request(AddUser, METHOD.POST, body)
}

/**
 * 更新用户信息
 * @param  {Int} companyID 公司ID
 * @param  {String} departmentName 可选 部门名称, 支持模糊匹配
 * @param  {String} userName  可选 用户名, 支持模糊匹配
 * @param  {String} cellPhone  可选 用户手机号码, 支持模糊匹配
 * @param  {Boolean} includeExternal 可选 是否包含外部用户
 * @param  {Int} pageSize
 * @param  {String} currentPage
 * @return {Promise<AxiosResponse<T>>}
 */
export async function updateUser(body) {
  return request(UpdateUser, METHOD.POST, body)
}

/**
 * 重置密码
 * @param  {Int} userID 用户id
 * @param  {String} newPassword 新密码
 * @param  {String} confirmPassword 重复输入密码
 * @return {Promise<AxiosResponse<T>>}
 */
export async function resetPassword(body) {
  return request(ResetPassword, METHOD.POST, body)
}

/**
 * 删除用户
 * @param  {Int} userID 用户id
 * @param  {String} newPassword 新密码
 * @param  {String} confirmPassword 重复输入密码
 * @return {Promise<AxiosResponse<T>>}
 */
export async function deleteUser(body) {
  return request(DeleteUser, METHOD.POST, body)
}

/**
 * 发送验证码
 * @param userID 用户ID
 * @param phone 手机号
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function sendSmsCode(body) {
  return request(SendSmsCode, METHOD.POST, body)
}

/**
 * 上传系统用户头像
 * @param userID 用户ID
 * @param phone 手机号
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function uploadUserAvatar(body) {
  return request(UploadUserAvatar, METHOD.POST, body)
}
