/*
 * @Author       : ly
 * @Date         : 2021-07-12 13:36:14
 * @LastEditors  : ly
 * @Description  : 外部用户
 */

import {
  QueryExternalPerson,
  DeleteCompanyExternalUser,
  BatchAddCompanyExternalUser,
  AddCompanyExternalUser,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 分页查询公司外部用户的信息
 * @param  {Int} companyID 公司ID
 * @param  {Int} pageSize
 * @param  {String} currentPage
 * @return {Promise<AxiosResponse<T>>}
 */
export async function queryExternalPerson(body) {
  return request(QueryExternalPerson, METHOD.POST, body)
}

/**
 *  删除公司外部用户
 * @param  {Int} companyID 公司ID
 * @param  {Int} userID 用户ID
 * @return {Promise<AxiosResponse<T>>}
 */
export async function deleteCompanyExternalUser(body) {
  return request(DeleteCompanyExternalUser, METHOD.POST, body)
}

/**
 *  添加公司外部用户
 * @param  {Int} companyID 公司ID
 * @param  {Int} groupIDList
 * @param  {DateTime} expireTime
 * @return {Promise<AxiosResponse<T>>}
 */
export async function addCompanyExternalUser(body) {
  return request(AddCompanyExternalUser, METHOD.POST, body)
}

/**
 *  批量添加公司外部用户
 * @param  {Int} companyID 公司ID
 * @param  {Int} addCompanyIDList
 * @param  {String} groupNameList
 * @param  {DateTime} expireTime
 * @return {Promise<AxiosResponse<T>>}
 */
export async function batchAddCompanyExternalUser(body) {
  return request(BatchAddCompanyExternalUser, METHOD.POST, body)
}
