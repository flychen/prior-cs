/*
 * @Author       : zp
 * @Date         : 2021-06-21 11:14:33
 * @LastEditors  : ly
 * @LastEditTime : 2021-07-27 18:04:34
 * @Description  : 描述
 */
import {
  QueryApplicationList,
  QueryApplication,
  DeleteApplication,
  UpdateApplication,
  AddApplication,
  ManageApplication,
} from '@/services/auth'
import { request, METHOD } from '@/utils/request'

/**
 * 查询公司内的应用列表(不分页列表)
 * @param {*} companyID
 * @param {*} appName
 * @param {*} pageSize
 * @param {*} currentPage
 * @returns
 */
export async function queryApplicationList(body) {
  return request(QueryApplicationList, METHOD.POST, body)
}

/**
 * 查询公司内的应用列表(分页列表)
 * @param {*} companyID
 * @param {*} appName
 * @param {*} pageSize
 * @param {*} currentPage
 * @returns
 */
export async function queryApplication(body) {
  return request(QueryApplication, METHOD.POST, body)
}

/**
 * 删除应用, 如果应用下有资源，则无法删除
 * @param {*} companyID
 * @param {Int[]} resourceGroupIDList 应用编号列表
 * @returns
 */
export async function deleteApplication(body) {
  return request(DeleteApplication, METHOD.POST, body)
}

/**
 * 更新应用
 * @param {*} companyID
 * @param {*} appID 应用ID
 * @param {*} appName 应用名称
 * @param {*} appVersion 应用版本
 * @param {*} appKey appKey
 * @param {*} appSecret appSecret
 *
 * @returns
 */
export async function updateApplication(body) {
  return request(UpdateApplication, METHOD.POST, body)
}

/**
 * 添加应用
 * @param {*} companyID
 * @param {*} appName 应用名称
 * @param {*} appVersion 应用版本
 * @param {*} appKey appKey
 * @param {*} appSecret appSecret
 *
 * @returns
 */
export async function addApplication(body) {
  return request(AddApplication, METHOD.POST, body)
}

/**
 * 管理应用下的权限
 * @param {*} companyID
 * @param {*} appID
 * @param {*} addPermissionIDList 添加权限ID列表
 * @param {*} removePermissionIDList 删除权限ID列表
 *
 * @returns
 */
export async function manageApplication(body) {
  return request(ManageApplication, METHOD.POST, body)
}
